CREATE TABLE player_balance (
    player_uid TEXT NOT NULL,
    balance INTEGER NOT NULL,
    PRIMARY KEY (player_uid)
);

CREATE TABLE player_gather_quest (
    player_uid TEXT NOT NULL,
    created_at INTEGER NOT NULL,
    duration INTEGER NOT NULL,
    material TEXT NOT NULL,
    item_quantity INTEGER NOT NULL,
    reward INTEGER NOT NULL,
    PRIMARY KEY (player_uid)
);

CREATE TABLE gather_quest_item_value (
    material TEXT NOT NULL,
    value INTEGER NOT NULL,
    PRIMARY KEY (material)
);

CREATE TABLE player_shop_price (
    player_uid TEXT NOT NULL,
    material TEXT NOT NULL,
    price INTEGER NOT NULL,
    PRIMARY KEY (player_uid)
);

INSERT INTO gather_quest_item_value (material, value) VALUES ("COAL", 50);
INSERT INTO gather_quest_item_value (material, value) VALUES ("RAW_IRON", 100);
INSERT INTO gather_quest_item_value (material, value) VALUES ("QUARTZ", 200);
INSERT INTO gather_quest_item_value (material, value) VALUES ("REDSTONE", 500);
INSERT INTO gather_quest_item_value (material, value) VALUES ("RAW_GOLD", 1000);
INSERT INTO gather_quest_item_value (material, value) VALUES ("LAPIS_LAZULI", 2000);
INSERT INTO gather_quest_item_value (material, value) VALUES ("DIAMOND", 5000);
INSERT INTO gather_quest_item_value (material, value) VALUES ("EMERALD", 10000);
