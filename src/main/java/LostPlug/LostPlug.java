package LostPlug;

import LostPlug.Commands.CommandsModule;
import LostPlug.Commands.ExecutorSetter.CommandsExecutorSetter;
import LostPlug.Database.DatabaseModule;
import LostPlug.Database.MigrationFailedException;
import LostPlug.Database.MigrationRunner;
import LostPlug.Listeners.ListenersModule;
import LostPlug.Listeners.PlayerGreeter.PlayerGreeter;
import LostPlug.Repositories.RepositoriesModule;
import LostPlug.Services.ServicesModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public class LostPlug extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        Logger logger = Bukkit.getLogger();

        Injector injector = Guice.createInjector(
                new DatabaseModule(),
                new RepositoriesModule(),
                new ServicesModule(),
                new CommandsModule(),
                new ListenersModule()
        );

        MigrationRunner migrationRunner = injector.getInstance(MigrationRunner.class);

        try {
            migrationRunner.runMigrations();
        } catch (MigrationFailedException e) {
            throw new RuntimeException(e);
        }

        logger.info("Database has been migrated.");

        Bukkit.getPluginManager().registerEvents(injector.getInstance(PlayerGreeter.class), this);

        injector.getInstance(CommandsExecutorSetter.class).setExecutors(this);
    }
//
//
//    @EventHandler
//    public void onInvClick(InventoryClickEvent event) {
//        if (event.getInventory().getHolder() != null) {
//            return;
//        }
//
//        event.setCancelled(true);
//
//        if (event.getAction() != InventoryAction.MOVE_TO_OTHER_INVENTORY) {
//            return;
//        }
//
//        HumanEntity player = event.getWhoClicked();
//
//        switch (((TextComponent) Objects.requireNonNull(event.getCurrentItem().lore()).get(0)).content().substring(2)) {
//            case "shop.create.confirm":
//                Block chest = (Block) player.getMetadata("lastShopChestAttemptBlock").get(0).value();
//
//                player.closeInventory();
//
//                chest.setMetadata("isShop", new FixedMetadataValue(this, player));
//                player.removeMetadata("lastShopChestAttemptBlock", this);
//
//                break;
//        }
//    }
//
//    @EventHandler
//    public void onInteract(PlayerInteractEvent event) {
//        Player player = event.getPlayer();
//        boolean isShiftPressed = player.isSneaking();
//        Action action = event.getAction();
//        Block block = event.getClickedBlock();
//
//        if (Objects.isNull(block)) {
//            return;
//        }
//
//        if (isShiftPressed && action == Action.RIGHT_CLICK_BLOCK && block.getType() == Material.CHEST) {
//            event.setCancelled(true);
//
//            if (block.getMetadata("isShop").isEmpty()) {
//                player.setMetadata("lastShopChestAttemptBlock", new FixedMetadataValue(this, block));
//
//                Inventory inv = Bukkit.createInventory(null, 3 * 9, Component.text("Create shop"));
//
//                ItemStack confirm = new ItemStack(Material.EMERALD);
//                ItemMeta confirmMeta = confirm.getItemMeta();
//                confirmMeta.displayName(Component.text("Confirm"));
//                confirmMeta.lore(List.of(
//                        Component.text(String.format("%sshop.create.confirm", ChatColor.WHITE)),
//                        Component.text(String.format("%s[Shift + Left click]%s to make this chest a shop", ChatColor.AQUA, ChatColor.WHITE))
//                ));
//                confirm.setItemMeta(confirmMeta);
//                inv.setItem(10, confirm);
//
//                player.openInventory(inv);
//            } else if (Objects.equals(block.getMetadata("isShop").get(0).value(), player)) {
//                Inventory inv = Bukkit.createInventory(null, 6 * 9, Component.text("Manage shop"));
//
//                short index = 3 * 9;
//                for (ItemStack originalItemStack : ((Chest) block.getState()).getInventory().getContents()) {
//                    if (originalItemStack == null) {
//                        continue;
//                    }
//
//                    ItemStack displayItemStack = new ItemStack(originalItemStack.getType());
//                    ItemMeta displayItemMeta = displayItemStack.getItemMeta();
//                    displayItemMeta.lore(List.of(
//                            Component.text(String.format("%sshop.set.price", ChatColor.WHITE)),
//                            Component.text(String.format("%s[Shift + Left click]%s to set the price", ChatColor.AQUA, ChatColor.WHITE))
//                    ));
//                    displayItemStack.setItemMeta(displayItemMeta);
//
//                    inv.setItem(index, displayItemStack);
//                    index += 1;
//                }
//
//                player.openInventory(inv);
//            }
//        }
//    }

}
