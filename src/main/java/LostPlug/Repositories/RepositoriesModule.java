package LostPlug.Repositories;

import LostPlug.Repositories.Balance.BalanceRepository;
import LostPlug.Repositories.Balance.DefaultBalanceRepository;
import LostPlug.Repositories.GatherQuest.DefaultGatherQuestRepository;
import LostPlug.Repositories.GatherQuest.GatherQuestRepository;
import com.google.inject.AbstractModule;

public class RepositoriesModule extends AbstractModule {
    @Override
    protected void configure() {
        super.configure();

        bind(BalanceRepository.class).to(DefaultBalanceRepository.class);
        bind(GatherQuestRepository.class).to(DefaultGatherQuestRepository.class);
    }
}