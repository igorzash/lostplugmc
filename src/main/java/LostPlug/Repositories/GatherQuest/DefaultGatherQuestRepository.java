package LostPlug.Repositories.GatherQuest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.UUID;

import LostPlug.Entities.GatherQuestEntity;
import LostPlug.Entities.GatherQuestItemValueEntity;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class DefaultGatherQuestRepository implements GatherQuestRepository {

    private final Connection conn;

    @Inject
    DefaultGatherQuestRepository (Connection conn) {
        this.conn = conn;
    }

    @Override
    public void clearGatherQuest(UUID playerUID) throws SQLException {
        Statement statement = conn.createStatement();
        statement.execute(String.format("DELETE FROM player_gather_quest WHERE player_uid='%s'", playerUID));
    }

    @Override
    public void insertGatherQuest(GatherQuestEntity gatherQuest) throws SQLException {
        Statement statement = conn.createStatement();
        statement.execute(String.format(
                "INSERT INTO player_gather_quest (player_uid, created_at, duration, material, item_quantity, reward) VALUES ('%s', %d, %d, '%s', %d, %d)",
                gatherQuest.playerUID(), gatherQuest.createdAt(), gatherQuest.duration(), gatherQuest.material(), gatherQuest.itemQuantity(), gatherQuest.reward()
        ));
    }

    @Override
    public GatherQuestItemValueEntity getRandomGatherQuestItem() throws SQLException {
        String sql = "SELECT material, value FROM gather_quest_item_value ORDER BY RANDOM() LIMIT 1";
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery(sql);

        GatherQuestItemValueEntity itemPrice;

        rs.next();
        itemPrice = new GatherQuestItemValueEntity(rs.getString("material"), rs.getInt("value"));

        return itemPrice;
    }

    @Override
    public Optional<GatherQuestEntity> getCurrentGatherQuest(UUID playerUID) throws SQLException {
        String sql = String.format(
                "SELECT created_at, duration, material, item_quantity, reward FROM player_gather_quest WHERE player_uid='%s' LIMIT 1",
                playerUID
        );
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery(sql);

        Optional<GatherQuestEntity> currentGatherQuest = Optional.empty();

        if (rs.next()) {
            currentGatherQuest = Optional.of(
                    new GatherQuestEntity(
                            playerUID,
                            rs.getLong("created_at"),
                            rs.getInt("duration"),
                            rs.getString("material"),
                            rs.getInt("item_quantity"),
                            rs.getInt("reward")
                    )
            );
        }

        return currentGatherQuest;
    }
}
