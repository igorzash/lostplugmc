package LostPlug.Repositories.GatherQuest;

import LostPlug.Entities.GatherQuestEntity;
import LostPlug.Entities.GatherQuestItemValueEntity;

import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

public interface GatherQuestRepository {
    void clearGatherQuest(UUID playerUID) throws SQLException;
    void insertGatherQuest(GatherQuestEntity gatherQuest) throws SQLException;
    GatherQuestItemValueEntity getRandomGatherQuestItem () throws SQLException;

    Optional<GatherQuestEntity> getCurrentGatherQuest (UUID playerUID) throws SQLException;
}
