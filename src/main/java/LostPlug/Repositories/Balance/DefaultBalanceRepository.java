package LostPlug.Repositories.Balance;

import com.google.inject.Inject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

public class DefaultBalanceRepository extends BalanceRepository {
    private final Connection conn;

    @Inject
    public DefaultBalanceRepository(Connection conn) {
        this.conn = conn;
    }

    private void initBalanceUnsafe(UUID playerUID) throws SQLException {
        String sql = String.format("INSERT INTO player_balance (player_uid, balance) VALUES ('%s', 0)", playerUID);
        Statement statement = conn.createStatement();
        statement.execute(sql);
    }
    @Override
    protected int initBalanceOrReturnExisting(UUID playerUID) throws SQLException {
        String sql = String.format("SELECT balance FROM player_balance WHERE player_uid='%s' LIMIT 1", playerUID);
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery(sql);

        if (!rs.isClosed()) {
            return rs.getInt("balance");
        } else {
            initBalanceUnsafe(playerUID);
            return 0;
        }
    }

    @Override
    public void credit(UUID playerUID, int amount) throws SQLException {
        super.credit(playerUID, amount);

        int newBalance = getBalance(playerUID) + amount;
        String sql = String.format("UPDATE player_balance SET balance=%d WHERE player_uid='%s'", newBalance, playerUID);
        Statement statement = conn.createStatement();
        statement.execute(sql);
    }
}
