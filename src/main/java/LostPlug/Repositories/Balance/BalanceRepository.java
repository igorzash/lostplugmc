package LostPlug.Repositories.Balance;

import java.sql.SQLException;
import java.util.UUID;

public abstract class BalanceRepository {
    protected abstract int initBalanceOrReturnExisting(UUID playerUID) throws SQLException;
    public int getBalance(UUID playerUID) throws SQLException {
        return initBalanceOrReturnExisting(playerUID);
    }
    public void credit (UUID playerUID, int amount) throws SQLException {
        assert amount > 0 && amount < 100000 * 100;
    }
}