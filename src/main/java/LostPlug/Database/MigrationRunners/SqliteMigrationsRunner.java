package LostPlug.Database.MigrationRunners;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import LostPlug.Database.MigrationFailedException;
import LostPlug.Database.MigrationRunner;
import LostPlug.LostPlug;
import com.google.inject.Inject;

public class SqliteMigrationsRunner implements MigrationRunner {

    private final Connection conn;

    @Inject
    public SqliteMigrationsRunner(Connection conn) {
        this.conn = conn;
    }

    private void createMigrationsTable() throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS latest_migration (id INT);";
        Statement statement = conn.createStatement();
        statement.execute(sql);
    }

    private Optional<Integer> getLatestMigrationId () throws SQLException {
        String sql = "SELECT id FROM latest_migration ORDER BY id DESC LIMIT 1;";
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery(sql);

        Optional<Integer> latestMigrationId = Optional.empty();

        while (rs.next()) {
            latestMigrationId = Optional.of(rs.getInt("id"));
        }

        return latestMigrationId;
    }

    public void runMigrations() throws MigrationFailedException {
        try {
            this.createMigrationsTable();
        } catch (SQLException e) {
            throw new MigrationFailedException("init migration table", e.toString());
        }

        Optional<Integer> latestMigrationId;
        try {
            latestMigrationId = getLatestMigrationId();
        } catch (SQLException e) {
            throw new MigrationFailedException("fetch last migration id", e.toString());
        }

        URL resource = LostPlug.class.getResource("/migrations");

        if (resource == null) {
            throw new MigrationFailedException("get migrations resource", "is null");
        }

        URI uri;
        try {
            uri = resource.toURI();
        } catch (URISyntaxException e) {
            throw new MigrationFailedException("get URI equivalent of the migration resource URL", e.toString());
        }

        FileSystem fileSystem;
        try {
            fileSystem = FileSystems.newFileSystem(uri, Collections.<String, Object>emptyMap());
        } catch (IOException e) {
            throw new MigrationFailedException("create filesystem using the migrations URI", e.toString());
        }

        Path migrationsPath = fileSystem.getPath("/migrations");

        Stream<Path> walk;
        try {
            walk = Files.walk(migrationsPath, 1);
        } catch (IOException e) {
            throw new MigrationFailedException("walk migration files", e.toString());
        }

        Iterator<Path> it = walk.iterator();
        it.next();

        while (it.hasNext()) {
            String migrationName = it.next().toString();
            int migrationId = Integer.parseInt(
                migrationName.substring("/migrations/".length()).split("-")[0]
            );

            if (latestMigrationId.isPresent() && latestMigrationId.get() >= migrationId) {
                continue;
            }

            System.out.println("Applying " + migrationName);

            try (InputStream in = getClass().getResourceAsStream(migrationName)) {
                assert in != null;

                try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
                    String[] sqlBatch = reader.lines().collect(Collectors.joining()).split(";");

                    Statement statement = conn.createStatement();

                    for (String batch : sqlBatch) {
                        String sql = batch.trim();

                        if (sql.length() > 0) {
                            statement.addBatch(sql);
                        }
                    }

                    statement.addBatch("DELETE FROM latest_migration");
                    statement.addBatch("INSERT INTO latest_migration (id) VALUES (" + migrationId + ")");

                    statement.executeBatch();

                    System.out.println("Applied " + migrationName);
                }
            } catch (IOException | SQLException e) {
                throw new MigrationFailedException(migrationName, e.toString());
            }
        }
    }

}
