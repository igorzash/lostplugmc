package LostPlug.Database;

public interface MigrationRunner {
    public void runMigrations() throws MigrationFailedException;
}
