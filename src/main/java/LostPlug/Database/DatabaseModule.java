package LostPlug.Database;

import LostPlug.Database.ConnectionProviders.SqliteConnectionProvider;
import LostPlug.Database.MigrationRunners.SqliteMigrationsRunner;
import com.google.inject.AbstractModule;

import java.sql.Connection;

public class DatabaseModule extends AbstractModule {

    @Override
    protected void configure() {
        super.configure();
        bind(Connection.class).toProvider(SqliteConnectionProvider.class);
        bind(MigrationRunner.class).to(SqliteMigrationsRunner.class);
    }

}