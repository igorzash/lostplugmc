package LostPlug.Database;

public class MigrationFailedException extends Exception {
    public MigrationFailedException(String migrationName, String errorMessage) {
        super(String.format("%s: %s", migrationName, errorMessage));
    }
}
