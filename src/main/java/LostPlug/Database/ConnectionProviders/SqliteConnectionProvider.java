package LostPlug.Database.ConnectionProviders;

import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Singleton
public class SqliteConnectionProvider implements Provider<Connection> {
    private Connection conn;
    @Override
    public Connection get() {
        if (conn != null) {
            return conn;
        }

        String url = "jdbc:sqlite:./lostplug.db";

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        Bukkit.getLogger().info("Successfully connected to the database");

        return conn;
    }
}
