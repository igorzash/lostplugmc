package LostPlug.Services.GatherQuest;

import LostPlug.Entities.GatherQuestEntity;

import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

public interface GatherQuestService {
    public GatherQuestEntity generateGatherQuest(UUID playerUID) throws SQLException;
    public Optional<GatherQuestEntity> getCurrentGatherQuestAndCheckExpiration (UUID playerId) throws SQLException;
    public GatherQuestEntity getPlayerCurrentGatherQuestOrGenerateNewOne(UUID playerId) throws SQLException;
}
