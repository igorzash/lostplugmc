package LostPlug.Services.GatherQuest;

import LostPlug.Entities.GatherQuestEntity;
import LostPlug.Entities.GatherQuestItemValueEntity;
import LostPlug.Repositories.GatherQuest.GatherQuestRepository;
import com.google.inject.Inject;

import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public class DefaultGatherQuestService implements GatherQuestService {
    private final GatherQuestRepository gatherQuestRepository;

    @Inject
    public DefaultGatherQuestService(GatherQuestRepository gatherQuestRepository) {
        this.gatherQuestRepository = gatherQuestRepository;
    }

    @Override
    public GatherQuestEntity generateGatherQuest(UUID playerUID) throws SQLException {
        GatherQuestItemValueEntity itemValue = gatherQuestRepository.getRandomGatherQuestItem();

        int itemQuantity = Math.min((int) Math.floor(50000.0 / itemValue.value()), 64);
        int reward = itemQuantity * itemValue.value();

        GatherQuestEntity gatherQuest = new GatherQuestEntity(
                playerUID,
                new Date().getTime(),
                1000 * 60 * 60,
                itemValue.material(),
                itemQuantity,
                reward
        );

        gatherQuestRepository.clearGatherQuest(playerUID);
        gatherQuestRepository.insertGatherQuest(gatherQuest);

        return gatherQuest;
    }

    @Override
    public Optional<GatherQuestEntity> getCurrentGatherQuestAndCheckExpiration (UUID playerId) throws SQLException {
        Optional<GatherQuestEntity> gatherQuestEntityOptional= this.gatherQuestRepository.getCurrentGatherQuest(playerId);

        if (gatherQuestEntityOptional.isEmpty()) {
            return Optional.empty();
        }

        GatherQuestEntity quest = gatherQuestEntityOptional.get();

        if (quest.createdAt() + quest.duration() - new Date().getTime() < 3000) {
            this.gatherQuestRepository.clearGatherQuest(playerId);
            return Optional.empty();
        }

        return gatherQuestEntityOptional;
    }

    @Override
    public GatherQuestEntity getPlayerCurrentGatherQuestOrGenerateNewOne(UUID playerId) throws SQLException {
        Optional<GatherQuestEntity> gatherQuestEntityOptional= this.gatherQuestRepository.getCurrentGatherQuest(playerId);

        boolean shouldGenerateNew = false;

        if (gatherQuestEntityOptional.isEmpty()) {
            shouldGenerateNew = true;
        } else {
            GatherQuestEntity quest = gatherQuestEntityOptional.get();

            if (quest.createdAt() + quest.duration() - new Date().getTime() < 3000) {
                shouldGenerateNew = true;
            }
        }

        if (shouldGenerateNew) {
            return generateGatherQuest(playerId);
        }

        return gatherQuestEntityOptional.get();
    }

}
