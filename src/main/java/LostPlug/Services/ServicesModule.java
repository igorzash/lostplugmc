package LostPlug.Services;

import LostPlug.Services.GatherQuest.DefaultGatherQuestService;
import LostPlug.Services.GatherQuest.GatherQuestService;
import com.google.inject.AbstractModule;

public class ServicesModule extends AbstractModule {
    @Override
    protected void configure() {
        super.configure();

        bind(GatherQuestService.class).to(DefaultGatherQuestService.class);
    }
}
