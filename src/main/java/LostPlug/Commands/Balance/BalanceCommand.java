package LostPlug.Commands.Balance;

import LostPlug.ChatComponents.ErrorChatComponentBuilder;
import LostPlug.Repositories.Balance.BalanceRepository;

import com.google.inject.Inject;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

public class BalanceCommand implements CommandExecutor {
    private final BalanceRepository balanceRepository;

    @Inject
    public BalanceCommand(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        if (sender instanceof Player player) {
            int balance;

            try {
                balance = this.balanceRepository.getBalance(player.getUniqueId());
            } catch (SQLException e) {
                player.sendMessage(new ErrorChatComponentBuilder().build());
                e.printStackTrace();

                return true;
            }

            player.sendMessage(new BalanceChatComponentBuilder().build(balance));
        }

        return true;
    }
}