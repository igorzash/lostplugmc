package LostPlug.Commands.Balance;

import net.kyori.adventure.text.Component;
import org.bukkit.ChatColor;

public class BalanceChatComponentBuilder {

    public Component build (int balance) {
        return Component.text(String.format("Balance: %s%d coins", ChatColor.GOLD, balance / 100));
    }

}
