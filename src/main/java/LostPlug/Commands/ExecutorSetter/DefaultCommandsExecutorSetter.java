package LostPlug.Commands.ExecutorSetter;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.java.JavaPlugin;

import javax.inject.Named;
import java.util.Objects;

@Singleton
public class DefaultCommandsExecutorSetter implements CommandsExecutorSetter {
    private final CommandExecutor balanceCommand;
    private final CommandExecutor gatherQuestCommand;
    private final CommandExecutor submitGatherQuestItemsCommand;

    @Inject
    public DefaultCommandsExecutorSetter(
            @Named("BALANCE_COMMAND") CommandExecutor balanceCommand,
            @Named("GATHER_QUEST_COMMAND") CommandExecutor gatherQuestCommand,
            @Named("SUBMIT_GATHER_QUEST_ITEMS_COMMAND") CommandExecutor submitGatherQuestItemsCommand
    ) {
        this.balanceCommand = balanceCommand;
        this.gatherQuestCommand = gatherQuestCommand;
        this.submitGatherQuestItemsCommand = submitGatherQuestItemsCommand;
    }
    @Override
    public void setExecutors(JavaPlugin plugin) {
        Objects.requireNonNull(plugin.getCommand("balance")).setExecutor(balanceCommand);
        Objects.requireNonNull(plugin.getCommand("gather_quest")).setExecutor(gatherQuestCommand);
        Objects.requireNonNull(plugin.getCommand("submit_gather_quest_items")).setExecutor(submitGatherQuestItemsCommand);
    }
}
