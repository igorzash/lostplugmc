package LostPlug.Commands;

import LostPlug.Commands.Balance.BalanceCommand;
import LostPlug.Commands.ExecutorSetter.CommandsExecutorSetter;
import LostPlug.Commands.ExecutorSetter.DefaultCommandsExecutorSetter;
import LostPlug.Commands.GatherQuest.GatherQuestCommand;
import LostPlug.Commands.GatherQuest.SubmitGatherQuestItemsCommand;
import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import org.bukkit.command.CommandExecutor;

public class CommandsModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(CommandsExecutorSetter.class).to(DefaultCommandsExecutorSetter.class);

        bind(CommandExecutor.class).annotatedWith(Names.named("BALANCE_COMMAND")).to(BalanceCommand.class);
        bind(CommandExecutor.class).annotatedWith(Names.named("GATHER_QUEST_COMMAND")).to(GatherQuestCommand.class);
        bind(CommandExecutor.class).annotatedWith(Names.named("SUBMIT_GATHER_QUEST_ITEMS_COMMAND")).to(SubmitGatherQuestItemsCommand.class);
    }
}
