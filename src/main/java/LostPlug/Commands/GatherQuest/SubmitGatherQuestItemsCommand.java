package LostPlug.Commands.GatherQuest;

import LostPlug.ChatComponents.ErrorChatComponentBuilder;
import LostPlug.Entities.GatherQuestEntity;
import LostPlug.Repositories.Balance.BalanceRepository;
import LostPlug.Repositories.GatherQuest.GatherQuestRepository;
import LostPlug.Services.GatherQuest.GatherQuestService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.Objects;
import java.util.Optional;

@Singleton
public class SubmitGatherQuestItemsCommand implements CommandExecutor {

    private final BalanceRepository balanceRepository;
    private final GatherQuestRepository gatherQuestRepository;
    private final GatherQuestService gatherQuestService;

    @Inject
    public SubmitGatherQuestItemsCommand(BalanceRepository balanceRepository, GatherQuestRepository gatherQuestRepository, GatherQuestService gatherQuestService) {
        this.balanceRepository = balanceRepository;
        this.gatherQuestRepository = gatherQuestRepository;
        this.gatherQuestService = gatherQuestService;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player player) {
            Optional<GatherQuestEntity> questOptional;
            GatherQuestEntity quest;

            try {
                if ((questOptional = gatherQuestService.getCurrentGatherQuestAndCheckExpiration(player.getUniqueId())).isEmpty()) {
                    player.sendMessage(new ErrorChatComponentBuilder().setError("You have no active gather quest! use /gather_quest first.").build());
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                player.sendMessage(new ErrorChatComponentBuilder().build());
                return true;
            }

            quest = questOptional.get();

            PlayerInventory inventory = player.getInventory();

            for (ItemStack itemStack : inventory) {
                if (itemStack == null) {
                    continue;
                }

                if (Objects.equals(itemStack.getType().toString(), quest.material()) && itemStack.getAmount() >= quest.itemQuantity()) {
                    if (itemStack.getAmount() == quest.itemQuantity()) {
                        inventory.removeItem(itemStack);
                    } else {
                        itemStack.setAmount(itemStack.getAmount() - quest.itemQuantity());
                    }

                    try {
                        gatherQuestRepository.clearGatherQuest(player.getUniqueId());
                    } catch (SQLException e) {
                        e.printStackTrace();
                        player.sendMessage(new ErrorChatComponentBuilder().build());
                        return true;
                    }

                    try {
                        balanceRepository.credit(player.getUniqueId(), quest.reward());
                    } catch (SQLException e) {
                        e.printStackTrace();
                        player.sendMessage(new ErrorChatComponentBuilder().build());
                        return true;
                    }

                    Bukkit.broadcast(Component.text(player.getName() + " has completed a gather quest!"));
                    player.sendMessage(Component.text(
                            String.format(
                                    "%sGather quest completed!",
                                    ChatColor.GREEN
                            )
                    ));

                    return true;
                }
            }

            player.sendMessage(Component.text(
                    String.format(
                        "%sInsufficient items!\n%s%sMake sure you have all the required items in your inventory!",
                        ChatColor.RED, ChatColor.WHITE, ChatColor.UNDERLINE
                    )
            ));
        }

        return true;
    }
}
