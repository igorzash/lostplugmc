package LostPlug.Commands.GatherQuest;

import LostPlug.ChatComponents.ErrorChatComponentBuilder;
import LostPlug.Entities.GatherQuestEntity;
import LostPlug.Services.GatherQuest.GatherQuestService;
import com.google.inject.Inject;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

public class GatherQuestCommand implements CommandExecutor {

    private final GatherQuestService gatherQuestService;

    @Inject
    public GatherQuestCommand(GatherQuestService gatherQuestService) {
        this.gatherQuestService = gatherQuestService;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        if (sender instanceof Player player) {
            GatherQuestEntity gatherQuest;

            try {
                gatherQuest = gatherQuestService.getPlayerCurrentGatherQuestOrGenerateNewOne(player.getUniqueId());
            } catch (SQLException e) {
                e.printStackTrace();
                player.sendMessage(new ErrorChatComponentBuilder().build());
                return true;
            }

            player.sendMessage(new GatherQuestChatComponentBuilder().build(gatherQuest));
        }

        return true;
    }
}
