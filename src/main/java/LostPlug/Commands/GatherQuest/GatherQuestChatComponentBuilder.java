package LostPlug.Commands.GatherQuest;

import LostPlug.Entities.GatherQuestEntity;
import LostPlug.Utils.CalculateTimeLeft;
import LostPlug.Utils.TimeDurationFormat;
import net.kyori.adventure.text.Component;
import org.bukkit.ChatColor;

public class GatherQuestChatComponentBuilder {
    public Component build (GatherQuestEntity gatherQuest) {
        return Component.text(String.format(
                "Collect: %s%sx%d%s; Reward: %s%d coins%s; Time left: %s%s%s",
                ChatColor.UNDERLINE, gatherQuest.material(), gatherQuest.itemQuantity(), ChatColor.WHITE,
                ChatColor.GOLD, gatherQuest.reward() / 100, ChatColor.WHITE,
                ChatColor.AQUA, TimeDurationFormat.format(CalculateTimeLeft.millisecondsLeft(gatherQuest.createdAt(), gatherQuest.duration())), ChatColor.WHITE
        ));
    }
}
