package LostPlug.ChatComponents;

import net.kyori.adventure.text.Component;
import org.bukkit.ChatColor;

public class ErrorChatComponentBuilder {

    private String error = "Server error!";

    public ErrorChatComponentBuilder setError(String error) {
        this.error = error;

        return this;
    }

    public Component build () {
        return Component.text(ChatColor.RED + error);
    }

}