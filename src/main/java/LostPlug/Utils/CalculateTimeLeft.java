package LostPlug.Utils;

import java.util.Date;

public class CalculateTimeLeft {
    public static long millisecondsLeft (long started, int duration) {
        return (started + duration) - new Date().getTime();
    }
}