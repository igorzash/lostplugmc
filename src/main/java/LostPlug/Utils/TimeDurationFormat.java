package LostPlug.Utils;

public class TimeDurationFormat {
    public static String format (long milliseconds) {
        if (milliseconds <= 1000) {
            return "0s";
        }

        int seconds = (int) Math.floor(milliseconds / 1000.0);

        if (milliseconds <= 60 * 1000) {
            return seconds + "s";
        }

        return ((int) Math.floor(seconds / 60.0)) + "min " + seconds % 60 + "s";
    }
}
