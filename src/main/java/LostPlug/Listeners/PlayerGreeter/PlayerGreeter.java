package LostPlug.Listeners.PlayerGreeter;

import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public interface PlayerGreeter extends Listener {
    public void onPlayerJoin(PlayerJoinEvent event);
}
