package LostPlug.Listeners.PlayerGreeter;

import net.kyori.adventure.text.Component;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public class SimplePlayerGreeter implements PlayerGreeter {
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.getPlayer().sendMessage(Component.text("Hey, " + event.getPlayer().getName() + "!"));
    }
}
