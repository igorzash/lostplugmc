package LostPlug.Listeners;

import LostPlug.Listeners.PlayerGreeter.PlayerGreeter;
import LostPlug.Listeners.PlayerGreeter.SimplePlayerGreeter;
import com.google.inject.AbstractModule;

public class ListenersModule extends AbstractModule {
    @Override
    protected void configure() {
        super.configure();

        bind(PlayerGreeter.class).to(SimplePlayerGreeter.class);
    }
}
