package LostPlug.Entities;

import org.bukkit.ChatColor;

import LostPlug.Utils.CalculateTimeLeft;
import LostPlug.Utils.TimeDurationFormat;

import java.util.UUID;

public record GatherQuestEntity(UUID playerUID, long createdAt, int duration, String material,
                                int itemQuantity,
                                int reward) {
}
