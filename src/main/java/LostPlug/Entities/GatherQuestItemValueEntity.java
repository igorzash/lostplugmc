package LostPlug.Entities;

public record GatherQuestItemValueEntity(String material, int value) {
}
