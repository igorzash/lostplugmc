package LostPlug.Entities;

public record PlayerShopPrice(String playerName, String itemName, int itemPrice) {
}
